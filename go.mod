module gitlab.com/micro-test/basic-micro-service

go 1.13

require (
	github.com/golang/protobuf v1.4.3
	github.com/micro/go-micro/v3 v3.0.0-beta.2.0.20200921154545-9dbd75f2cc13
	github.com/micro/go-plugins/broker/nats/v3 v3.0.0-20200908121001-4ea6f6760baf // indirect
	github.com/micro/go-plugins/events/stream/nats/v3 v3.0.0-20200908121001-4ea6f6760baf // indirect
	github.com/micro/go-plugins/metrics/prometheus/v3 v3.0.0-20200908121001-4ea6f6760baf // indirect
	github.com/micro/go-plugins/registry/etcd/v3 v3.0.0-20200908121001-4ea6f6760baf // indirect
	github.com/micro/go-plugins/store/cockroach/v3 v3.0.0-20200908121001-4ea6f6760baf // indirect
	github.com/micro/micro/v3 v3.0.0-beta.7
	github.com/micro/services v0.10.0 // indirect
	github.com/stripe/stripe-go/v71 v71.28.0 // indirect
	google.golang.org/grpc v1.27.0
	google.golang.org/protobuf v1.25.0
)

replace google.golang.org/grpc => google.golang.org/grpc v1.26.0
