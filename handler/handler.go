package handler

import (
	"context"
	"fmt"
)

type Example struct{}

type Request struct {
	Name string
}

type Response struct {
	Msg string
}

// Call is a single request handler called via client.Call or the generated client code
func (e *Example) Call(ctx context.Context, req *Request, rsp *Response) error {
	fmt.Println("Received Example.Call request")
	rsp.Msg = "Hello " + req.Name
	return nil
}
